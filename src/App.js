import React, { Component } from 'react';
import axios from 'axios';
import {Grid, Row, Glyphicon, Button} from 'react-bootstrap';
import './App.css';
import Post from './components/Post';
import PostDetailsModel from './components/PostDetailsModel';
import loader from './loader.gif'
import logo1 from './1.png'

class App extends Component {
  state = {
    posts : [],
    show : false,
    fileUpload : false,
    imgFile : null,
    id : 0
  }

  componentDidMount(){
    axios.get(`http://starlord.hackerearth.com/insta`)
      .then(res => {
        const posts = res.data;
        console.log(posts);
        this.setState({ posts });
      })
  } 
  
  showModel = (id) => { this.setState({ show : true, id }) }
  hideModel = () => { this.setState({ show : false }) }

  showImgUploadModel = () => { 
    this.setState({
      fileUpload : true
    })
  }

  hideImgUploadModel = () => { 
    this.setState({
      fileUpload : false
    })
  }
  
  addPost = () => {
    let post = {}
    post.Image = this.state.imgFile
    post.likes = 0
    post.timestamp = new Date()
    let posts = [...this.state.posts, post]
    if(this.state.imgFile !== null){
      this.setState({
        posts, imgFile : null
      },()=>{this.hideImgUploadModel()})
    }
  }

  deletePost = (id) => {
    let posts = [...this.state.posts]
    posts.splice(id, 1)
    this.setState({
      posts
    },()=>{this.hideModel()})
  }

  editInfo = (id, msg) => {
    let posts = [...this.state.posts]
    let post = posts[id]
    if(post.hasOwnProperty('label')){
      post.label = msg
    } else {
      post['label'] =msg
    } 
    posts[id] = post
    
    this.setState({
      posts
    })
  }

  likeCount = (likeValue, postId, likeState) => {
    let posts = [...this.state.posts]
    let post = posts[postId]
    post.likes += likeValue
    post['likeState'] = likeState
    posts[postId] = post
    
    this.setState({
      posts
    })
  }

  addComment = (postId, comment) => {
    let posts = [...this.state.posts]
    let post = posts[postId]
    if(post.hasOwnProperty('comments')){
      post.comments.push(comment)
    } else {
      post['comments'] = [comment]
    } 
    posts[postId]=post
    this.setState({ posts })
  }

  deleteComment = (postId, commentId) => {
    let posts = [...this.state.posts]
    let post = posts[postId]
    post.comments.splice(commentId, 1)
    posts[postId]=post
    this.setState({ posts })
  }

  onChange = event => {
    this.setState({
      imgFile : URL.createObjectURL(event.target.files[0])
    })
  }

  render() {
    const {posts} = this.state 
    if(!posts.length) 
      return (
        <div className="data-loading">
          <img src={loader} alt="loading" className="loader-img"/>
        </div>)
    else   
      return (
        <div className="App">
          <header className="App-header">
            InstaApp
          </header>
          <div className="profile">
            <div>
              <img className="profile-img" alt="profile"
                src={logo1}/>
              <h2 className="user-name">Username</h2>
              <h5 className="user-posts"> 
              {this.state.posts.length} posts
              </h5>
              <h5 className="user-posts1"> # followers </h5>
              <h5 className="user-posts2"> # following </h5>
            </div> 
            <div className="upload-img">
              <input 
                className="upload-file"
                type='file'
                accept=".gif,.jpg,.jpeg,.png"
                onChange={this.onChange}/> 
              <Button 
                className="upload-btn"
                bsSize="small"
                onClick={this.addPost}>
                <Glyphicon glyph="plus"/> Upload
              </Button>
            </div>
          </div>
          <hr/>
          <Grid className="grid">
            <Row className="show-grid card">
              {
                posts.map((post,index) => 
                  <Post 
                    key={index}
                    id={index}
                    showModel={this.showModel}
                    likes={post.likes}
                    comments={post.comments ? post.comments.length : 0}
                    image={post.Image}/>)
              }
            </Row>
          </Grid>

          <PostDetailsModel 
            id = {this.state.id}
            posts={this.state.posts}
            editInfo={this.editInfo}
            deletePost={this.deletePost}
            addComment={this.addComment}
            deleteComment={this.deleteComment}
            likeCount={this.likeCount}
            show={this.state.show}
            onHide={this.hideModel}/> 
        </div>
      );
  }
}

export default App;
